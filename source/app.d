import std.stdio;
import std.conv;
import std.array;
import consoled:getch;

enum memory_size=1024;
enum filename="mandelbrot.bf";

string Compile()
{
    const code=import(filename);
    string ret;
    size_t[] jumpstack;
    size_t[code.length] jumptable;
    
    for(size_t i=0;i<code.length;i++)
    {
        if(code[i]=='[')
        {
            jumpstack~=i;
        }
        else if(code[i]==']')
        {
            size_t backto=jumpstack.back;
            jumpstack.length-=1;
            jumptable[i]=backto;
            jumptable[backto]=i;
        }
    }
    
    ret~="char memory[memory_size]=0;\nsize_t pointer=0;\n";
    for(size_t i=0;i<code.length;i++)
	{
	    if(code[i]=='+')
	        ret~="memory[pointer]+=1;\n";
        else if(code[i]=='-')
	        ret~="memory[pointer]-=1;\n";
        else if(code[i]=='>')
	        ret~="pointer+=1;\n";
        else if(code[i]=='<')
	        ret~="pointer-=1;\n";
        else if(code[i]=='.')
	        ret~="write(memory[pointer]);\n";
        else if(code[i]=='[')
            ret~="if(memory[pointer]==0) goto end"~jumptable[i].to!string()~";\nstart"~i.to!string()~": ";
        else if(code[i]==']')
            ret~="if(memory[pointer]!=0) goto start"~jumptable[i].to!string()~";\nend"~i.to!string()~": ";
        else if(code[i]==',')
            ret~="memory[pointer]=cast(char)getch();\n";
	}
	ret~="return;";
    return ret;
}

void Run()
{
    enum res=Compile();
	mixin(res);
}

void main()
{
	Run();
}
